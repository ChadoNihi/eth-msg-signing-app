export const validateMessage = message => {
  if (message.length) {
    return true;
  } else {
    alert('Error: message must not be empty.');
  }
}