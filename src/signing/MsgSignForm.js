import React, {
  PureComponent
} from 'react';
import {
  connect
} from 'react-redux';
import PropTypes from 'prop-types';
import {
  Button,
  Form
} from 'semantic-ui-react';
import Web3 from 'web3';

import {
  ADD_MSG,
  SET_IS_SIGNING
} from '../store/actions';
import {
  validateMessage
} from './helpers.js';

class MsgSignForm extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      passwordSoFar: '',
      messageSoFar: ''
    };

    this.web3 = this.props.web3;
    this.onMessageChange = this.onMessageChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.onSign = this.onSign.bind(this);
  }

  onMessageChange(ev) {
    this.setState({
      messageSoFar: ev.target.value
    });
  }

  onPasswordChange(ev) {
    this.setState({
      passwordSoFar: ev.target.value
    });
  }

  async onSign(ev) {
    const message = this.state.messageSoFar.trim();

    ev.preventDefault();

    if (validateMessage(message)) {
      this.props.setIsSigning(true);

      let acc, signature;
      try {
        acc = (await this.web3.eth.getAccounts())[0];
        const pass = this.state.passwordSoFar;
        signature = await this.web3.eth.personal.sign(message, acc, pass);
      } catch (e) {
        this.props.setIsSigning(false);

        console.error(e);
        alert(
          'Error on signing the message. Try again with enabled MetaMask or in a Ethereum browser.'
        );

        return;
      }

      this.props.addMsg({
        message,
        signature
      });

      this.props.setIsSigning(false);

      this.setState({
        messageSoFar: ''
      });
    }
  }

  render() {
    return (
      <PresentationalMsgSignForm
        isSigning={this.props.isSigning}
        messageSoFar={this.state.messageSoFar}
        onMessageChange={this.onMessageChange}
        onPasswordChange={this.onPasswordChange}
        onSign={this.onSign}
        passwordSoFar={this.state.passwordSoFar}
      />
    );
  }
}

MsgSignForm.propTypes = {
  isSigning: PropTypes.bool.isRequired,
  msgs: PropTypes.arrayOf(PropTypes.shape({
    message: PropTypes.string.isRequired,
    signature: PropTypes.string.isRequired
  })),
  web3: PropTypes.instanceOf(Web3)
};

const PresentationalMsgSignForm = ({
  isSigning,
  messageSoFar,
  onMessageChange,
  onPasswordChange,
  onSign,
  passwordSoFar
}) => (
  <Form onSubmit={onSign}>
    <Form.Field
      label='Message'
      cols='52'
      control='textarea'
      onChange={onMessageChange}
      placeholder='A message to sign...'
      required={true}
      rows='6'
      value={messageSoFar}
    />
    <Form.Field
      label='Ethereum account password'
      control='input'
      onChange={onPasswordChange}
      placeholder='Could be your MetaMask password...'
      type='password'
      required={true}
      value={passwordSoFar}
    />
    <Button
      type='submit'
      disabled={messageSoFar.trim().length === 0 || passwordSoFar.length === 0}
      loading={isSigning}
      primary>
        Sign & Save
    </Button>
  </Form>
);

PresentationalMsgSignForm.propTypes = {
  isSigning: PropTypes.bool.isRequired,
  messageSoFar: PropTypes.string.isRequired,
  onMessageChange: PropTypes.func.isRequired,
  onPasswordChange: PropTypes.func.isRequired,
  onSign: PropTypes.func.isRequired,
  passwordSoFar: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  isSigning: state.msgSigningReducer.isSigning,
  msgs: state.msgsReducer.msgs
});

const mapDispatchToProps = dispatch => ({
  addMsg: msg => {
    dispatch({
      type: ADD_MSG,
      payload: {
        ...msg,
        tm: Date.now()
      }
    });
  },
  setIsSigning: bool => {
    dispatch({
      type: SET_IS_SIGNING,
      payload: bool
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(MsgSignForm);