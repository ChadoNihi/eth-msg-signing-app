import {
  SET_IS_SIGNING
} from '../store/actions';

const defaultState = {
  isSigning: false
};

export default (state = defaultState, action) => {
  switch (action.type) {
  case SET_IS_SIGNING:
    return {
      ...state,
      isSigning: action.payload
    };

  default:
    return state;
  }
};