import React from 'react';
import {
  Menu
} from 'semantic-ui-react';

import './footer.css';
import Logo from './Logo';

const Footer = () => (
  <footer className='footer'>
    <Menu className='content' inverted>
      <Menu.Item>
        <a href='/'><Logo /></a>
      </Menu.Item>
    </Menu>
  </footer>
);

export default Footer;