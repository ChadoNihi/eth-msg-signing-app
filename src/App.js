import React, {
  Component,
  Fragment
} from 'react';
import {
  Container,
  Grid
} from 'semantic-ui-react';
import Web3 from 'web3';

import Footer from './layout/Footer';
import Header from './layout/Header';
import Page from './layout/Page';

import MsgSignForm from './signing/MsgSignForm';
import MsgList from './msg-listing/MsgList';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      web3: new Web3(
        Web3.givenProvider ||
        new Web3.providers.HttpProvider('http://localhost:8545') ||
        'ws://localhost:8546'
      )
    };
  }

  render() {
    return (
      <Fragment>
        <Container text>
          <Header />

          <Page>
            <Grid centered>
              <Grid.Row>
                <MsgSignForm web3={this.state.web3} />
              </Grid.Row>
              <Grid.Row>
                <MsgList web3={this.state.web3} />
              </Grid.Row>
            </Grid>
          </Page>
        </Container>

        <Footer />
      </Fragment>
    );
  }
}

export default App;