export const ADD_MSG = 'ADD_MSG';
export const REPLACE_MSGS = 'REPLACE_MSGS';
export const DELETE_MSG = 'DELETE_MSG';

export const SET_IS_SIGNING = 'SET_IS_SIGNING';

export const SET_IS_VERIFYING = 'SET_IS_VERIFYING';