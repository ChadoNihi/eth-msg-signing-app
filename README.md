A React-Redux-[Web3](https://github.com/ethereum/web3.js/) app for one interview assignment.

### Todos
- [ ] Warn on the same message-signature pair
- [ ] Configure max line length for eslint
- [ ] Refactor the footer
- [ ] Write tests (and follow TDD henceforth)
- [ ] Add contrast mode (should be easy w/ Semantic UI React's `inverted` attribute)
- [ ] Better feedback on errors
- [ ] Implement sorting and [searching](https://react.semantic-ui.com/modules/search)
- [ ] Add pagination
- [ ] Automate favicon generation (see e.g. [favicons-webpack-plugin](https://www.npmjs.com/package/favicons-webpack-plugin))
- [ ] PWA compliant (i.e. installable, offline working, etc.)

## Run locally
1) `git clone https://gitlab.com/ChadoNihi/eth-msg-signing-app.git && cd eth-msg-signing-app`
2) Install dependencies with `npm install` (or `yarn install`). (See the dependencies in `package.json`.)
3) Run the app in development mode with `npm start` (or `yarn start`). (Open [http://localhost:3000](http://localhost:3000) to view it in the browser.) To build the app for production: `npm run build` (or `yarn build`). (See all available commands in Create React App's [docs.](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#available-scripts).)

## Tests
Run tests with `npm test` (or `yarn test`).
